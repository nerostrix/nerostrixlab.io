---
title: "Introduction"
summary: "Now that this blog is up and running..."
date: 2022-05-01
tags: ["Introduction", "DARKEST HOUR"]
categories: ["Regular posts"]
author: "nerostrix"
---

Hi everyone! Welcome to my blog. Here I'll write about my current personal projects, and any progress or updates related to them.

As you may already know, I'm currently working on a RPG-style videogame. The tentative title of this project is *DARKEST HOUR*. The game is still in a very early development phase, and there's no fixed release date for now, but you can rest assured that all relevant progress will be published here on this blog!

For those who didn't see it back in the day, here's an early mockup I made for the dialogue screens:

![An early mockup of the dialogue screen. It shows a sample dialogue, along with the name of the character speaking and a face sprite of that same character on the left.](img/ui-mockup-early.png)

Keep in mind that this is just the beginning. There's more, **muuuuch more** to come!

If you want to hear about any new updates, you can [follow me on Twitter](https://twitter.com/nerostrix), where I will share any new posts published on this blog! My Twitter DMs are open in case you want to contact me, too.