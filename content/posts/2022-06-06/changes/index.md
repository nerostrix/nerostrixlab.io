---
title: "Changes"
summary: "The hiatus will continue, but this doesn't mean the development is 100% paralyzed. Also, trans rights are human fucking rights."
date: 2022-06-06
tags: ["DARKEST HOUR"]
categories: ["Regular posts"]
author: "nerostrix"
---

Hey everyone. I have some good and bad news regarding *DARKEST HOUR* and the [development hiatus announced on May 14](/posts/2022-05-14/development-hiatus).

First of all, **the hiatus will continue for about a month**. I'm sorry about this, but right now I need to take care of my mental health as well as other important issues. As much as I want to continue actively working on DH, I just can't afford it at this moment. Really sorry about that.

However, there's some good news as well. After a friend of mine managed to get me a [Toyhouse](https://toyhou.se/) invite code, I've been slowly focusing on the character design, personality and backstory aspects of the project. As I explained on my Twitter account, these aspects are not as affected by the development hiatus:

{{< tweet user="nerostrix" id="1530087152440188930" >}}

At this moment, **I'm working on the character backstories, personality descriptions and designs**. I will probably show the new character designs on [my Twitter account](https://twitter.com/nerostrix), so feel free to follow me if you want to stay tuned!

---

This would've been all there is to this post. However, I'd like to take a minute to discuss a couple important topics.

First of all. As you may already know, I'm a part of the LGBTQ community myself. And I strongly believe that trans rights are human rights. I am, and always will be, fiercely and uncompromisingly supportive of my community, and this of course includes my trans comrades.

**No, I'm not willing to debate any of this**. Argue with the fucking wall.

In the past few days, prominent *gender-critical* activist Helen Joyce [has been quoted](https://twitter.com/chimenesuleyman/status/1532841418972078085) as saying that every single person who undergoes medical transition is "a huge problem to a sane world", as well as "a difficulty", and that "the fewer of those people are, the better". Additionally, the barrage of anti-trans laws, [including an Ohio bill targeting trans athletes and allowing for genital inspections on kids](https://www.usatoday.com/story/sports/columnist/nancy-armour/2022/06/04/ohio-bill-targets-transgender-athletes-with-genital-check/7504997001/), has only intensified in the last weeks.

From this little, almost invisible platform of mine, I would like to state that **my heart goes to all the trans people facing this inhumane treatment all over the world. You have my wholehearted support.**

I would also like to announce something I have never mentioned before, other than on my private and personal account, which only my closer friends have access to:

As a bisexual, queer and non-binary person, it is my desire to offer healthy and realistic LGBTQ representation in *DARKEST HOUR*. The protagonists are four queer young people, and yes, they will be represented as openly and unmistakably queer throughout the game. More specifically, the group consists of **a bisexual man**, **a trans gay man**, **a sapphic girl** and **a bisexual non-binary person**.

Even though I have yet to finish the designs, this is the only one I've finished so far, and I think it's the perfect time to reveal it.

{{< lead >}}
**This is Felix, a trans gay man.**
{{< /lead >}}

{{< figure
    src="img/felix-says-trans-rights.png"
	alt="New design for the character"
	caption="Design made with [this Picrew](https://picrew.me/image_maker/1269502)"
	class="img-50percent"
	height=256
    >}}

With Felix, I hope not only to offer **proper transmasc representation**, but also to remind everyone that **trans people can also be gay, lesbian or bisexual**. This one goes to all my gay and bisexual transmasc folks. I see you, I love you and I want to help amplify your voices, so that everyone can hear your stories.

**More information about Felix, as well as the other characters, will be coming in the next weeks.** Thanks for reading, have a nice day ❤️
