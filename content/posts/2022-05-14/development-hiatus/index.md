---
title: "Development hiatus"
summary: "Development of DARKEST HOUR will be halted temporarily."
date: 2022-05-14
tags: ["Hiatus", "DARKEST HOUR"]
categories: ["Announcements"]
author: "nerostrix"
---

Hey everyone. I'm sorry to announce that I'll be taking a short break from working on *DARKEST HOUR*, in order to focus on personal stuff.

This is not to say I'm abandoning the project. But there's other issues I need to take care of, and this project is just something I'm doing for the fun of it. It's not a part of my job. It's not even a commercial project. Therefore, as much as I want to work on it, I can't afford taking it as my top priority.

I expect this hiatus to last for about a month, but it might be shorter or longer. Don't worry, I'll let you know when the development is resumed.