---
title: "About"
summary: "About this site, and the author."
showAuthor: false
showDate: false
---

In this blog you will find updates, teasers and announcements related to my projects. Every now and then I might even post different stuff, such as personal thoughts and observations. Please keep in mind that I'm not, by any means, an "actual" blogger. What I mean by this is, my blog has a very specific *raison d'être*, and that's the only thing you should expect from it.

This site is made with [Hugo](https://gohugo.io). The source code is [available on GitLab](https://gitlab.com/nerostrix/nerostrix.gitlab.io).


## The author

{{< figure
    src="img/nerostrix-avatar-512px.png"
    alt="The avatar used by Nerostrix"
    caption="Avatar made with [this Picrew](https://picrew.me/image_maker/1269502)!"
    class="img-50percent"
    height=256
    >}}

Hi there! I'm **Nero**. If you're reading this, chances are you already know me, but for those who don't, allow me to introduce myself.

I'm a 21-year-old guy from Spain who enjoys writing and coding in his free time. I go by <u>*he/they*</u> pronouns. I speak Spanish *(my native language)* and English. My hobbies include videogames, listening to music and, to a lesser extent, roleplaying. Cats are one of my favorite things in this world... although they don't seem to like me as much as I like them. I also drink a humongous amount of energy drinks. <del>Stay away from energy drinks, kids, I can tell you they're no good.</del>

From a very early age, I developed a strong interest in computers and videogames. *Metal Gear Solid 3*, which I played when I was relatively young, is perhaps the main reason why I love videogames. Not only did I learn that you can use the medium to tell a story; the game also proved that you can intertwine the different elements ---story, gameplay, artistic elements--- in a way that a more "static" medium like a book doesn't allow you to.

Throughout the years, I ended up realizing that I wanted to do the same.


## Why be a gamedev?

I know, I know. The videogame industry is so massive nowadays that us gamedevs need to be ready to face so much competition. As a youngblood, I'm aware there's a lot of people who release stuff that isn't just similar to mine, but also superior and much more deserving of praise than my stuff.

So what?

I don't do it for the money. I don't do it for the accolades or the worldwide recognition. I sure don't expect to make a living out of videogame development; in fact, as long as I can afford to do it, I'm more than willing to make financial sacrifices in order to carry out my projects. There's no way in hell I'd ever choose to work as a "professional" videogame developer.

Why am I here, then? Simple. I want to make videogames that are emotionally fulfilling for me. This is nothing but a hobby to me. I write stories that I might eventually turn into a videogame. Of course, you gotta pay the bills; or, more precisely, you gotta pay the people you work with, and you gotta pay for other stuff, like servers. That's why I wish to rely on crowdfunding to release my projects. I want to set the prices as low as possible, so that they can be played by more people. And, if anything, I want the majority of the earnings to go to my collaborators; artists, composers... you know.

To sum it up --- I'm here to do what I like, test my own limits as a writer and programmer, and maybe letting the world know how amazingly talented my peers are!


## Current projects

As you may know, I'm working on a short, somewhat experimental RPG called [*DARKEST HOUR*](/tags/darkest-hour/). There's no fixed release date, but I'll eventually let you know when it's close to being finished!

Additionally, after that one, I have a couple more projects I might want to work on. It'll also depend on the reception *DARKEST HOUR* is met with. For now, all I can give is two working titles: *ALPHA OMEGA* and *EVEN//TIDE*.


## Where to find me

I have [a Twitter account](https://twitter.com/nerostrix) that I also use to talk about my work and share any new updates on this blog. If you're curious about my other open-source software projects, you can also check out [my GitLab account](https://gitlab.com/nerostrix)!
