# My silly little blog

This is the source code for [my personal blog](https://nerostrix.gitlab.io).

## Usage

If you're familiar with [Hugo](https://gohugo.io), you probably know what to do with this. It's essentially a Hugo-based project that uses the [Congo](https://github.com/jpanther/congo) theme.

### Requisites

- `git`
- `hugo`

### Obtaining the code

First, clone this repository.

```sh
git clone https://gitlab.com/nerostrix/nerostrix.gitlab.io
```

Next, **inside the repository root folder**, run the following commands to download the [Congo](https://github.com/jpanther/congo) theme as well:

```
git submodule init
git submodule update --remote --merge
```

Now you should be good to go!

### File structure

#### Posts

All the published posts can be found in the [`content/posts/`](/content/posts) folder. The content of each post is contained in an entire folder, not just an HTML/Markdown file; this is what the Hugo documentation calls [page bundles](https://gohugo.io/content-management/page-bundles/#leaf-bundles) For example, the folder corresponding to my first post, "*Introduction*", is structured like this:

```
introduction/
├── img/
│   └── ui-mockup-early.png
└── index.md
```

Note that each post folder is itself contained in another folder, named as the publish date in `YYYY-MM-DD` format, to avoid problems like name clashes:

```
.
├── content/
│   ├── 2022-05-01/
│   │   ├── introduction/
│   │   │   ├── img/
│   │   │   │   └── ui-mockup-early.png
│   │   │   └── index.md
│   ├── 2022-05-14/
│   │   ├── development-hiatus/
│   │   │   └── index.md
(...)
```

#### Special pages

Right now there's only one of these "special pages", called *About*. The internal structure of the folders corresponding to these pages would be the same as shown before:

```
about/
├── img/
│   └── avatar-early.png
└── index.md
(...)
```

The difference is that, unlike posts, the folder containing these pages is placed in the [`content/`](/content) folder directly:

```
.
├── content/
│   ├── about/
│   │   ├── img/
│   │   │   └── avatar-early.png
│   │   └── index.md
(...)
```

They don't have an actual publish date because they're not posts in the strict sense. For the same reason, the `showAuthor` variable is set to *false*, so that the default author name is not displayed right under the title.

### Deploying to GitLab Pages

This repository comes with a `.gitlab-ci.yml` file that defines the CI/CD jobs, so simply pushing to GitLab should be enough. After the build passes, your page will be available.

Before pushing, don't forget to change the `baseURL` parameter on the [`config.toml`](/config/_default/config.toml) file to the corresponding URL! Otherwise, you'll run into problems such as the page not rendering properly or the links being broken. [This document by GitLab](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html) showcases the default values for that base URL.

For more information about deploying to GitLab, please refer to [this guide](https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/).

### Deploying to other platforms

If GitLab ain't your cup of tea, that's okay! It's also possible to host your page on Azure, AWS, GitHub, Netlify, Cloudflare... The Hugo website has [several guides](https://gohugo.io/hosting-and-deployment/) you can check out. If your preferred platform is in the list, follow the corresponding guide to deploy and build your site.

## License

This repository is [publicly available on GitLab](https://gitlab.com/nerostrix/nerostrix.gitlab.io) under the [MIT License](/LICENSE).
